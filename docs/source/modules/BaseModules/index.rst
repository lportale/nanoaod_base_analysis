BaseModules
===========

Installation
------------

``git clone https://gitlab.cern.ch/cms-phys-ciemat/cmt-base-modules.git Base/Modules``

RDFModules
----------

.. automodule:: Base.Modules 

.. toctree::
    :maxdepth: 2
   
    baseModules
